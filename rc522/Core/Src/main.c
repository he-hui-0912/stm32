/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "string.h"
#include "stdio.h"
#include "rc522.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */


typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */


#define RC522_CS_PIN GPIO_PIN_4
#define RC522_CS_PORT GPIOA



/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart3;

PCD_HandleTypeDef hpcd_USB_OTG_FS;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_USB_OTG_FS_PCD_Init(void);
static void MX_SPI1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

#ifdef __GNUC__
int __io_putchar(int ch)
#else
int fputc(int ch, FILE *f)
#endif
{
    HAL_UART_Transmit(&huart3, (uint8_t *)&ch, 1, 0xFFFF);
    return ch;
}


u8 MFRC522_ToCard(u8 command, u8 *sendData, u8 sendLen, u8 *backData,
 		uint16_t *backLen) ;

void MFRC522_CalculateCRC(u8 *pIndata, u8 len, u8 *pOutData) ;

//void  print(char *content);
//
//void  print2(u8 a) ;

/**
 * Abstraction on the format used to define the address bytes sent to the MFRC522
 * as part of an SPI transfer
 */
struct address_byte {
	u8 lsb :1; // 0
	u8 addr :6;
	u8 rw :1; // 1 -> Read, 0 -> Write
} __packed;

struct address_byte address_byte_build(u8 mode, u8 addr) {
	struct address_byte byte = { .rw = mode, .addr = addr, };
	return byte;
}


int spi_write(u8 *data, u16 size) {
	if(HAL_OK == HAL_SPI_Transmit(&hspi1,data,size,HAL_MAX_DELAY)){
		return 0;
	} else return -1;
}

int mfrc522_register_write(u8 reg, u8 value) {
	struct address_byte reg_write = address_byte_build(0, reg);
	u8 data[2] = { 0, value };

	// We cannot directly put reg_write in data[0] at init time
	memcpy(&data[0], &reg_write, sizeof(u8));
	return spi_write(data, 2);
}

int spi_write_then_read(u8 *tx,u8 *rx,u16 size){
	if(HAL_OK == HAL_SPI_TransmitReceive(&hspi1,tx,rx,size,HAL_MAX_DELAY))
		return 0;
	else return -1;
}


int mfrc522_register_read(u8 reg, u8 *read_buff,
		u8 read_len) {
	size_t i;
	int ret;
	struct address_byte reg_read = address_byte_build(1, reg);

	for (i = 0; i < read_len; i++) {
		ret = spi_write_then_read((u8*)&reg_read, read_buff + i, 1);
		if (ret < 0)
			return ret;
	}

	return read_len;
}

void MFRC522_WriteRegister(u8 reg, u8 value) {
	mfrc522_register_write(reg, value);
}

u8 MFRC522_ReadRegister(u8 reg) {
	u8 tmp = 0;
//	 HAL_GPIO_WritePin(RC522_CS_PORT, RC522_CS_PIN, GPIO_PIN_RESET);
//	 HAL_Delay(1);
	mfrc522_register_read(reg, &tmp, 1);
//	HAL_Delay(1);
//	  HAL_GPIO_WritePin(RC522_CS_PORT, RC522_CS_PIN, GPIO_PIN_SET);
	return tmp;
}

void MFRC522_SetBitMask(u8 reg, u8 mask) {
	MFRC522_WriteRegister(reg, MFRC522_ReadRegister(reg) | mask);
}

void MFRC522_ClearBitMask(u8 reg, u8 mask) {
	MFRC522_WriteRegister(reg, MFRC522_ReadRegister(reg) & (~mask));
}

void MFRC522_AntennaOn(void) {
	u8 temp;
	temp = MFRC522_ReadRegister(MFRC522_REG_TX_CONTROL);
	if (!(temp & 0x03))
		MFRC522_SetBitMask(MFRC522_REG_TX_CONTROL, 0x03);
}

void MFRC522_AntennaOff(void) {
	MFRC522_ClearBitMask(MFRC522_REG_TX_CONTROL, 0x03);
}

void MFRC522_Reset(void) {
	MFRC522_WriteRegister(MFRC522_REG_COMMAND, PCD_RESETPHASE);  //  0x0F
}

 void mfrc522_fifo_flush(void) {
	u8 flush_byte = 1 << MFRC522_FIFO_LEVEL_REG_FLUSH_SHIFT;

	mfrc522_register_write( MFRC522_FIFO_LEVEL_REG, flush_byte);
}

 int mfrc522_fifo_write(const u8 *buf, size_t len) {
	size_t i;
	int ret;

	for (i = 0; i < len; i++) {
		ret = mfrc522_register_write( MFRC522_FIFO_DATA_REG,
				buf[i]);
		if (ret < 0)
			return ret;
	}

	return 0;
}

 u8 readVersionReg(void) {

 	return  MFRC522_ReadRegister(MFRC522_REG_VERSION);
 }


 /** Detect if the device we are talking to is an MFRC522 using the VersionReg,
  * section 9.3.4.8 of the datasheet
  *
  * @client SPI device
  *
  * @return -1 if not an MFRC522, version number otherwise
  */
 static int mfrc522_detect() {
 	u8 version = readVersionReg();
	printf("[MFRC522] version : %d\n",version);
 	switch (version) {
 	case MFRC522_VERSION_1:
 	case MFRC522_VERSION_2:
 		version = MFRC522_VERSION_NUM(version);
 		printf("[MFRC522] MFRC522 version detected,ok \n");
 		return version;
 	default:
 		printf("[MFRC522] this chip is not an MFRC522\n");
 		return -1;
 	}

 }

 /**
  * 新卡的出厂密码一般是密码A和密码B都是6个FF.
  */
 uint8_t MFRC522_Anticoll(uint8_t *serNum) {
 	uint8_t status;
 	uint8_t i;
 	uint8_t serNumCheck = 0;
 	uint16_t unLen;

 	MFRC522_WriteRegister(MFRC522_REG_BIT_FRAMING, 0x00);// TxLastBists = BitFramingReg[2..0]
 	serNum[0] = PICC_ANTICOLL;
 	serNum[1] = 0x20;
 	status = MFRC522_ToCard(PCD_TRANSCEIVE, serNum, 2, serNum, &unLen);
 	if (status == MI_OK) {
 		// Check card serial number
 		for (i = 0; i < 4; i++)
 			serNumCheck ^= serNum[i];
 		if (serNumCheck != serNum[i])
 			status = MI_ERR;
 	}
 	return status;
 }


 u8 MFRC522_ToCard(u8 command, u8 *sendData, u8 sendLen, u8 *backData,
 		uint16_t *backLen) {
 	u8 status = MI_ERR;
 	u8 irqEn = 0x00;
 	u8 waitIRq = 0x00;
 	u8 lastBits;
 	u8 n;
 	uint16_t i;

 	switch (command) {
 	case PCD_AUTHENT: {
 		irqEn = 0x12;
 		waitIRq = 0x10;
 		break;
 	}
 	case PCD_TRANSCEIVE: {
 		irqEn = 0x77;
 		waitIRq = 0x30;
 		break;
 	}
 	default:
 		break;
 	}

 	MFRC522_WriteRegister(MFRC522_REG_COMM_IE_N, irqEn | 0x80);
 	MFRC522_ClearBitMask(MFRC522_REG_COMM_IRQ, 0x80);
 	MFRC522_SetBitMask(MFRC522_REG_FIFO_LEVEL, 0x80);
 	MFRC522_WriteRegister(MFRC522_REG_COMMAND, PCD_IDLE);

 	// Writing data to the FIFO
 	for (i = 0; i < sendLen; i++)
 		MFRC522_WriteRegister(MFRC522_REG_FIFO_DATA, sendData[i]);

 	// Execute the command
 	MFRC522_WriteRegister(MFRC522_REG_COMMAND, command);
 	if (command == PCD_TRANSCEIVE)
 		MFRC522_SetBitMask(MFRC522_REG_BIT_FRAMING, 0x80);// StartSend=1,transmission of data starts

 	// Waiting to receive data to complete
 	i = 50;	//i according to the clock frequency adjustment, the operator M1 card maximum waiting time 25ms
 	printf("waitIRq :  0x%x", waitIRq);
 	do {
 		HAL_Delay(1);
 		// CommIrqReg[7..0]
 		// Set1 TxIRq RxIRq IdleIRq HiAlerIRq LoAlertIRq ErrIRq TimerIRq
 		n = MFRC522_ReadRegister(MFRC522_REG_COMM_IRQ);
 		printf("MFRC522_REG_COMM_IRQ :  0x%x", n);
 		i--;
 	} while ((i != 0) && !(n & 0x01) && !(n & waitIRq));

 	printf("i :  %d", i);

 	if (i == 0) {
 		printf("error");
 	}

 //	if(1) return 0 ;

 	MFRC522_ClearBitMask(MFRC522_REG_BIT_FRAMING, 0x80);		// StartSend=0

 	if (i != 0) {
 		if (!(MFRC522_ReadRegister(MFRC522_REG_ERROR) & 0x1B)) {
 			status = MI_OK;
 			if (n & irqEn & 0x01)
 				status = MI_NOTAGERR;
 			if (command == PCD_TRANSCEIVE) {
 				n = MFRC522_ReadRegister(MFRC522_REG_FIFO_LEVEL);

 				printf("FIFO bytes :  %d", n);

 				lastBits = MFRC522_ReadRegister(MFRC522_REG_CONTROL) & 0x07;
 				if (lastBits)
 					*backLen = (n - 1) * 8 + lastBits;
 				else
 					*backLen = n * 8;
 				if (n == 0)
 					n = 1;
 				if (n > MFRC522_MAX_LEN)
 					n = MFRC522_MAX_LEN;
 				for (i = 0; i < n; i++)
 					backData[i] = MFRC522_ReadRegister(MFRC522_REG_FIFO_DATA);// Reading the received data in FIFO
 			}
 		} else
 			status = MI_ERR;
 	}
 //	pr_info("backLen :  %d",*backLen);
 	printf("MFRC522_ToCard end, status:%d \n", status);
 	return status;
 }



 u8 MFRC522_Request(u8 reqMode, u8 *TagType) {
 	u8 status;
 	uint16_t backBits;								// The received data bits

// 	pr_info("MFRC522_Request start\n");
 	MFRC522_WriteRegister(MFRC522_REG_BIT_FRAMING, 0x07);// TxLastBists = BitFramingReg[2..0]
 	TagType[0] = reqMode;
 	status = MFRC522_ToCard(PCD_TRANSCEIVE, TagType, 1, TagType, &backBits);
 	if ((status != MI_OK) || (backBits != 0x10))
 		status = MI_ERR;

 	if(status == MI_ERR) {
 		printf("MFRC522_Request error \n");
 	}
 	else {
 		printf("MFRC522_Request ok \n");
 	}

// 	print("MFRC522_Request end, status:%d \n", status);
 	return status;
 }

 /**
  * S50.pdf, page 6 .
  *
  */
 u8 MFRC522_Read(u8 block_addr) {
 	u8 status;
 	uint16_t unLen;
 	int i;
 	u8 recvData[18] = { };
 	recvData[0] = PICC_READ;
 	recvData[1] = block_addr;
 	MFRC522_CalculateCRC(recvData, 2, &recvData[2]);
 	status = MFRC522_ToCard(PCD_TRANSCEIVE, recvData, 4, recvData, &unLen);
 	printf("MFRC522_Read unLen: %d\n", unLen);
 	if ((status != MI_OK) || (unLen != 0x90)) {
 		status = MI_ERR;
 		printf("MFRC522_Read status fail: %d\n", status);
 		return status;
 	}

 	for (i = 0; i < 18; i++) {
 		printf("recvData_%d: 0x%x\n", i, recvData[i]);
 	}
 	printf("MFRC522_Read status: %d\n", status);
 	return status;
 }

 u8 MFRC522_decrment_value_block(u8 blockAddr) {
 	u8 writeData[16] = { };
 	u8 status;
 	u32 pValue = 1;
 	uint16_t unLen = 0;
 	writeData[0] = PICC_DECREMENT;
 	writeData[1] = blockAddr;
 	MFRC522_CalculateCRC(writeData, 2, &writeData[2]);
 	status = MFRC522_ToCard(PCD_TRANSCEIVE, writeData, 4, writeData, &unLen);

 	printf("writeData[0]: 0x%x\n",writeData[0]);

 	if ((status != MI_OK)  || (unLen!=4) || ((writeData[0] & 0x0F) != 0x0A)) {
 		status = MI_ERR;
 	}

 	if (status == MI_OK) {
 		printf("ok  1\n");
 		memcpy(writeData, &pValue, 4);
 		MFRC522_CalculateCRC(writeData, 4, &writeData[4]);
 		unLen = 0;
 		status = MFRC522_ToCard(PCD_TRANSCEIVE, writeData, 6, writeData,
 				&unLen);
 		if (status != MI_ERR) {
 			status = MI_OK;
 		}
 	}

 	if (status == MI_OK) {
 		printf("ok 2\n");
 		writeData[0] = PICC_TRANSFER;
 		writeData[1] = blockAddr;
 		unLen = 0;
 		MFRC522_CalculateCRC(writeData, 2, &writeData[2]);
 		status = MFRC522_ToCard(PCD_TRANSCEIVE, writeData, 4, writeData,
 				&unLen);

 		if ((status != MI_OK) || (unLen != 4)
 				|| ((writeData[0] & 0x0F) != 0x0A)) {
 			status = MI_ERR;
 		}
 	}
 	printf("MFRC522_decrment_value_block status: %d \n", status);
 	return status;
 }


 void MFRC522_CalculateCRC(u8 *pIndata, u8 len, u8 *pOutData) {
 	u8 i, n;

 	MFRC522_ClearBitMask(MFRC522_REG_DIV_IRQ, 0x04);			// CRCIrq = 0
 	MFRC522_SetBitMask(MFRC522_REG_FIFO_LEVEL, 0x80);// Clear the FIFO pointer
 	// Write_MFRC522(CommandReg, PCD_IDLE);

 	// Writing data to the FIFO
 	for (i = 0; i < len; i++)
 		MFRC522_WriteRegister(MFRC522_REG_FIFO_DATA, *(pIndata + i));
 	MFRC522_WriteRegister(MFRC522_REG_COMMAND, PCD_CALCCRC);

 	// Wait CRC calculation is complete
 	i = 0xFF;
 	do {
 		n = MFRC522_ReadRegister(MFRC522_REG_DIV_IRQ);
 		i--;
 	} while ((i != 0) && !(n & 0x04));							// CRCIrq = 1

 	// Read CRC calculation result
 	pOutData[0] = MFRC522_ReadRegister(MFRC522_REG_CRC_RESULT_L);
 	pOutData[1] = MFRC522_ReadRegister(MFRC522_REG_CRC_RESULT_M);
 }

 /**
  * Authentication操作的卡应答以AE位给出，在PCD中由Status2Reg寄存器第4位标志，0为失败，1为成�????????????????.

  使用格式�????????????????1B模式 + 1B块地�???????????????? + 6B密码 + 4B卡片序列�????????????????

  块地�????????????????：指定非接触卡存储中某一个KEYSET
  模式：分A密码和B密码认证，A命令�????????????????0x60，B命令�????????????????0X61
  密码：需要视非接触卡情况而定，一般初始使用时密码均为F
  卡序列号：从防冲突中获取
  返回值：认证成功status寄存器对应位�????????????????1�????????????????
  *
  *  新卡的出厂密码A和密码B都是6个FF.
  *
  *  MFRC522.PDF  page 74  .
  *
  *
  *
  *
  */
 u8 MFRC522_Auth(u8 *uid, u8 block_addr) {
 	u8 status = 0;
 	uint16_t backBits; // The received data bits
 	u8 cmd[12] = { };

 //	模式：分A密码和B密码认证，A命令�????????????????0x60，B命令�????????????????0X61
 	cmd[0] = 0x61;
 	cmd[1] = block_addr;
 	cmd[2] = 0xFF;
 	cmd[3] = 0xFF;
 	cmd[4] = 0xFF;
 	cmd[5] = 0xFF;
 	cmd[6] = 0xFF;
 	cmd[7] = 0xFF;
 	cmd[8] = uid[0];
 	cmd[9] = uid[1];
 	cmd[10] = uid[2];
 	cmd[11] = uid[3];

 	printf("MFRC522_Auth start\n");
 //	MFRC522_WriteRegister(MFRC522_REG_BIT_FRAMING, 0x00); // TxLastBists = BitFramingReg[2..0]
 	status = MFRC522_ToCard(PCD_AUTHENT, cmd, 12, cmd, &backBits);
 	if (status == MI_OK && (MFRC522_ReadRegister(MFRC522_REG_STATUS2) & 0x08)) {
 		printf("MFRC522_Auth end, success\n");
 		status = MFRC522_Read(block_addr);
 		if (status == MI_OK) {
 //			status = MFRC522_Write(block_addr);
 //			status = MFRC522_generate_value_block(block_addr);
 //
 //			if (status == MI_OK) {
 				status = MFRC522_decrment_value_block(block_addr);
 //			}

 			if (status == MI_OK) {
 				status = MFRC522_Read(block_addr);
 			}
 		}
 	} else {
 		printf("MFRC522_Auth end, fail\n");
 		status = MI_ERR;
 	}
 	return status;
 }



 /**
  * https://blog.csdn.net/weixin_42889383/article/details/103892749
  */
 u8 MFRC522_Select(u8 *uid) {
 	u8 cmd[9] = { };
 	u8 status;
 	uint16_t backBits; // The received data bits
 	cmd[0] = PICC_SElECTTAG;
 	cmd[1] = 0x70;
 	cmd[2] = uid[0];
 	cmd[3] = uid[1];
 	cmd[4] = uid[2];
 	cmd[5] = uid[3];
 	cmd[6] = uid[4];
 	MFRC522_CalculateCRC(cmd, 7, &cmd[7]);
 	printf("MFRC522_Select start\n");
 	MFRC522_WriteRegister(MFRC522_REG_BIT_FRAMING, 0x00); // TxLastBists = BitFramingReg[2..0]
 	status = MFRC522_ToCard(PCD_TRANSCEIVE, cmd, 9, cmd, &backBits);
 	if ((status != MI_OK) || (backBits != 24)) {
 		printf("MFRC522_Select end,  error\n");
 		status = MI_ERR;
 		return status;
 	}
 	printf("cmd 0: 0x%x \n", cmd[0]);
 	printf("MFRC522_Select end, status:%d \n", status);
 	status = MFRC522_Auth(uid, 4);
 	return status;
 }


 u8 MFRC522_Check(void) {
 	u8 id[16] = { };
 	u8 status;
 	printf("MFRC522_Check start \n");

 	printf("send REQA\n");

 	/**
 	 *   fcd-14443-3.pdf
 	 *   6.4.2
 	 */
 	status = MFRC522_Request(PICC_REQA, id);	// Find cards, return card type

// 	pr_info("received ATQA:0x%x", id[0]);
 //	pr_info("received ATQA:0x%x",id[1]);

// 	pr_info("UID size:0x%x", (id[0] & 0b11000000) >> 6);

 	if (status != MI_OK) {
 		printf("no card detected \n");
 		return -1;
 	}

 	printf("start Anticollision \n");

 //	防冲突：0x93�????????????????0x20，返回得�????????????????4B的卡ID + 1B校验(异或)
 	status = MFRC522_Anticoll(id);// Card detected. Anti-collision, return card serial number 4 bytes
 //	MFRC522_Halt();    // Command card into hibernation

 	printf("id : %x,%x,%x,%x,%x\n", id[0], id[1], id[2], id[3], id[4]);
 //	pr_info("id : %x,%x,%x,%x,%x\n", id[5],id[6],id[7],id[8],id[9]);
 	if (status == MI_OK) {
 		status = MFRC522_Select(id);
 	}

 	return status;
 }

 void MFRC522_Init(void) {
 	MFRC522_Reset();
// 	printk(KERN_INFO "after Reset: CommIRqReg: 0x%x\n",
// 	printf(MFRC522_ReadRegister(MFRC522_REG_COMM_IRQ));
// 			;
//
// 	printk(KERN_INFO "after Reset: MFRC522_REG_T_MODE: 0x%x\n",
// 			MFRC522_ReadRegister(MFRC522_REG_T_MODE));
// 	printk(KERN_INFO "after Reset: MFRC522_REG_T_PRESCALER: 0x%x\n",
// 			MFRC522_ReadRegister(MFRC522_REG_T_PRESCALER));
// 	printk(KERN_INFO "after Reset: MFRC522_REG_T_RELOAD_H: 0x%x\n",
// 			MFRC522_ReadRegister(MFRC522_REG_T_RELOAD_H));
// 	printk(KERN_INFO "after Reset: MFRC522_REG_T_RELOAD_L: 0x%x\n",
// 			MFRC522_ReadRegister(MFRC522_REG_T_RELOAD_L));

 //	printk(KERN_INFO "677 = 0x%x\n",677);
 //	printk(KERN_INFO "0xd3e = %d\n",0xd3e);

 	MFRC522_WriteRegister(MFRC522_REG_T_MODE, 0x8D);
 	MFRC522_WriteRegister(MFRC522_REG_T_PRESCALER, 0x3E);
 	MFRC522_WriteRegister(MFRC522_REG_T_RELOAD_L, 30);
 	MFRC522_WriteRegister(MFRC522_REG_T_RELOAD_H, 0);

 //	MFRC522_WriteRegister(MFRC522_REG_T_MODE, 0x0D);
 //	MFRC522_WriteRegister(MFRC522_REG_T_PRESCALER, 0x3E);
 //	MFRC522_WriteRegister(MFRC522_REG_T_RELOAD_L, 0);
 //	MFRC522_WriteRegister(MFRC522_REG_T_RELOAD_H, 0);

 	MFRC522_WriteRegister(MFRC522_REG_RF_CFG, 0x70);				// 48dB gain
 	MFRC522_WriteRegister(MFRC522_REG_TX_AUTO, 0x40);

 	MFRC522_WriteRegister(MFRC522_REG_MODE, 0x3D);
 	MFRC522_AntennaOn();
// 	printk(KERN_INFO "after init 1: CommIRqReg: 0x%x\n",
// 			MFRC522_ReadRegister(MFRC522_REG_COMM_IRQ));
 }

// uint8_t  buffer[1024]={0};
// void  print(char *s1){
//			int s = sprintf((char*) buffer, s1);
//			HAL_UART_Transmit(&huart3, buffer, s, HAL_MAX_DELAY);
// }
//
// void  print2(u8 a){
//			int s = sprintf((char*) buffer, "0x%x\n", a);
//			HAL_UART_Transmit(&huart3, buffer, s, HAL_MAX_DELAY);
// }



/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART3_UART_Init();
  MX_USB_OTG_FS_PCD_Init();
  MX_SPI1_Init();
  /* USER CODE BEGIN 2 */
//    int i = 0 ;
    while (mfrc522_detect() == -1) {
    	HAL_Delay(1000);
    }

    HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin,1);

//
//    printf("after mfrc522_detect\n");
//
//
//    	MFRC522_Init();
//    	MFRC522_Check();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    		u8 tx = 123;
    		u8 rx = 0;
    	HAL_SPI_TransmitReceive(&hspi1,&tx,&rx,1,HAL_MAX_DELAY);
    	if(rx==123){
  printf("ok\n");
	HAL_Delay(500);
	rx =0 ;
    	}

//    		mfrc522_detect();
		HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_0);
//		HAL_Delay(1000);

	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 72;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_HARD_OUTPUT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * @brief USB_OTG_FS Initialization Function
  * @param None
  * @retval None
  */
static void MX_USB_OTG_FS_PCD_Init(void)
{

  /* USER CODE BEGIN USB_OTG_FS_Init 0 */

  /* USER CODE END USB_OTG_FS_Init 0 */

  /* USER CODE BEGIN USB_OTG_FS_Init 1 */

  /* USER CODE END USB_OTG_FS_Init 1 */
  hpcd_USB_OTG_FS.Instance = USB_OTG_FS;
  hpcd_USB_OTG_FS.Init.dev_endpoints = 6;
  hpcd_USB_OTG_FS.Init.speed = PCD_SPEED_FULL;
  hpcd_USB_OTG_FS.Init.dma_enable = DISABLE;
  hpcd_USB_OTG_FS.Init.phy_itface = PCD_PHY_EMBEDDED;
  hpcd_USB_OTG_FS.Init.Sof_enable = ENABLE;
  hpcd_USB_OTG_FS.Init.low_power_enable = DISABLE;
  hpcd_USB_OTG_FS.Init.lpm_enable = DISABLE;
  hpcd_USB_OTG_FS.Init.vbus_sensing_enable = ENABLE;
  hpcd_USB_OTG_FS.Init.use_dedicated_ep1 = DISABLE;
  if (HAL_PCD_Init(&hpcd_USB_OTG_FS) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USB_OTG_FS_Init 2 */

  /* USER CODE END USB_OTG_FS_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LD1_Pin|LD3_Pin|LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(USB_PowerSwitchOn_GPIO_Port, USB_PowerSwitchOn_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : PE2 PE4 PE13 PE14 */
  GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_4|GPIO_PIN_13|GPIO_PIN_14;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF5_SPI4;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : USER_Btn_Pin */
  GPIO_InitStruct.Pin = USER_Btn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USER_Btn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : RMII_MDC_Pin RMII_RXD0_Pin RMII_RXD1_Pin */
  GPIO_InitStruct.Pin = RMII_MDC_Pin|RMII_RXD0_Pin|RMII_RXD1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : RMII_REF_CLK_Pin RMII_MDIO_Pin */
  GPIO_InitStruct.Pin = RMII_REF_CLK_Pin|RMII_MDIO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LD1_Pin LD3_Pin LD2_Pin */
  GPIO_InitStruct.Pin = LD1_Pin|LD3_Pin|LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : RMII_TXD1_Pin */
  GPIO_InitStruct.Pin = RMII_TXD1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(RMII_TXD1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_PowerSwitchOn_Pin */
  GPIO_InitStruct.Pin = USB_PowerSwitchOn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(USB_PowerSwitchOn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_OverCurrent_Pin */
  GPIO_InitStruct.Pin = USB_OverCurrent_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USB_OverCurrent_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : RMII_TX_EN_Pin RMII_TXD0_Pin */
  GPIO_InitStruct.Pin = RMII_TX_EN_Pin|RMII_TXD0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
